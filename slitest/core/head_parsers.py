from django.utils.six import iteritems


def init(self, **kwargs):
    for k, v in iteritems(kwargs):
        setattr(self, k, v)

try:
    from lxml import html as lxhtml
    from StringIO import StringIO

    class HeadHTMLParser:
        def __init__(self, lookup_tags=None):
            init(self, **{'lookup_tags': lookup_tags or ['title']})

        document = lambda self, data: lxhtml.parse(StringIO(data))

        def yielded_parse(self, data):
            for tag in self.lookup_tags:
                for element in self.document(data).iter(tag):
                    yield {tag: element.text_content()}

        def parse(self, data):
            dc = {}
            for item in self.yielded_parse(data):
                dc.update(**item)
            return dc


except ImportError:
    from HTMLParser import HTMLParser as VanillaHTMLParser

    class HeadHTMLParser(VanillaHTMLParser):
        def __init__(self, lookup_tags=None):
            VanillaHTMLParser.__init__(self)
            init(self, **{'lookup_tags': lookup_tags or ['title'],
                          'lookup_tag_opened': False,
                          'current_tag': None,
                          'dc': dict()
            })

        def feed(self, data):
            VanillaHTMLParser.feed(self, data.decode('utf-8'))

        def handle_starttag(self, tag, attrs):
            if tag in self.lookup_tags:
                self.lookup_tag_opened = True

        def handle_endtag(self, tag):
            self.lookup_tag_opened = False

        def handle_data(self, data):
            if self.lookup_tag_opened:
                self.dc.update({self.current_tag: data})

        def parse(self, data):
            self.feed(data)
            return self.dc

headParser = HeadHTMLParser()