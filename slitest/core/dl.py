import urllib2
import Queue
import multiprocessing as python_mp
from urlparse import urlparse
import time

from django.utils.six import iteritems

to_download = python_mp.Queue()
resulted = python_mp.Queue()


class Downloader:
    def __init__(self, max_try=3, max_process=3):
        self.MAX_TRY = max_try
        self.MAX_PROCESS = max_process

    add_urls_to_queue = lambda self, urls: self._urls_to_queue(urls)

    @staticmethod
    def _urls_to_queue(urls=None):
        urls = urls if isinstance(urls, (list, tuple)) else [urls]
        map(to_download.put, filter(None, urls))

    @staticmethod
    def normalize(url):
        parsed = urlparse(url)
        norm_url = '{scheme}://{netloc}{path}'.format(**{
            'scheme': parsed.scheme or 'http',
            'path': parsed.path or '/',
            'netloc': parsed.netloc
        })
        if parsed.query:
            norm_url += '?%s' % parsed.query
        return norm_url

    def start_async(self, urls=None):
        self.add_urls_to_queue(urls)

        for x in range(1, self.MAX_PROCESS):
            p = python_mp.Process(target=self.start)
            p.start()

    def start(self, urls=None):
        self.add_urls_to_queue(urls)

        while to_download:
            try:
                url = to_download.get_nowait()
            except Queue.Empty:
                time.sleep(1)
                continue

            resulted.put(self._download(url))

    def _download(self, url, cnt=0):
        if cnt >= self.MAX_TRY:
            return

        req = urllib2.Request(self.normalize(url))

        try:
            res = urllib2.urlopen(req).read()
        except (urllib2.HTTPError, urllib2.URLError):
            cnt += 1
            return self._download(url, cnt)
        return dict([(url, res)])

dl = Downloader()

if __name__ == '__main__':
    dl.start_async(['ya.ru', 'python.org', 'ru.wikipedia.org'])

    import time
    from slitest.core.head_parsers import headParser

    def test_title_attr(*args, **kwargs):
        while not resulted.empty():
            for url, data in iteritems(resulted.get()):
                print url, headParser.parse(data)

    pool = python_mp.Pool(processes=dl.MAX_PROCESS)
    mp = pool.map_async(time.sleep, (3,), callback=test_title_attr)
    mp.wait()