"""
WSGI config for slitest project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "slitest.settings")


def wsgi_app():
    from django.core.wsgi import get_wsgi_application
    from slitest.core.dl import dl

    dl.start_async()
    return get_wsgi_application()

application = wsgi_app()