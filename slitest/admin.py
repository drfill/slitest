from django.contrib import admin
from slitest.models.websites import TagsData


class SLITestAdminInline(admin.TabularInline):
    model = TagsData
    extra = 0
    readonly_fields = ('tag', 'data')


class SLITestAdmin(admin.ModelAdmin):
    exclude = ('data',)
    list_display = ('website', 'allow_download')
    actions = ['update_info']
    inlines = [SLITestAdminInline]

    update_info = lambda self, *args, **kwargs: TagsData.update_info()