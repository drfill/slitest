from django.utils.six import iteritems


class TempStore(object):
    def __init__(self, seq=None, **kwargs):
        self.db = seq or {}
        for k, v in kwargs.items():
            self.set(k, v)

    def get(self, item, default=None):
        return self.db.get(item, default)

    def set(self, key, value):
        self.db[key] = value

    def add(self, item, value):
        if not item and not value:
            return
        obj = self.get(item, None)
        if obj is None:
            obj = value if value else None
        elif isinstance(obj, list):
            obj.append(value if isinstance(value, list) else [])
        elif isinstance(obj, tuple):
            obj = tuple(list(obj) + list(value) if isinstance(value, tuple) else list(obj))
        elif isinstance(obj, dict):
            obj.update(value if isinstance(value, dict) else {})
        if item and obj:
            self.set(item, obj)

    def __iter__(self):
        return iteritems(self.db)

tempStorage = TempStore()