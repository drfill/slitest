from django.apps import AppConfig
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from slitest.admin import SLITestAdmin
from slitest.models.websites import Websites


class SLITestConfig(AppConfig):
    name = 'slitest'
    verbose_name = _("SLITEST HOMEWORK")

    def ready(self):
        admin.site.register(Websites, SLITestAdmin)