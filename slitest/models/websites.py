import re

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.six import iteritems
from slitest.core.dl import dl, resulted
from slitest.core.head_parsers import headParser


class Websites(models.Model):
    website = models.URLField(u'Website', unique=True, db_index=True)
    tags = models.CharField(u'Tags', help_text=u'Divided by comma', default='title', max_length=52)
    allow_download = models.BooleanField(u'Download?', default=True)
    data = models.TextField(u'Data')

    class Meta:
        verbose_name, verbose_name_plural = u'Website', u'Websites'

    def __unicode__(self):
        return self.website

    def get_tags(self):
        return filter(None, self.tags.replace(' ', '').split(','))

    def save(self, *args, **kwargs):
        self.tags = ''.join(re.findall(r'[\s|a-zA-Z|,|_|-]+', self.tags))
        super(Websites, self).save(*args, **kwargs)
        if self.allow_download:
            dl.add_urls_to_queue(self.website)


class TagsData(models.Model):
    website = models.ForeignKey(Websites, related_name='tagsData', on_delete=models.CASCADE)
    tag = models.CharField(u'Tag', max_length=52, db_index=True)
    data = models.TextField(u'Data', null=True, blank=True)

    class Meta:
        unique_together = (("website", "tag"),)

    @classmethod
    def update_info(cls):
        while not resulted.empty():
            for url, data in iteritems(resulted.get()):
                for tag, tag_data in iteritems(headParser.parse(data)):
                    cls.objects.filter(website__website=url, tag=tag).update(data=tag_data)


@receiver(post_save, sender=Websites)
def create_update_data_in_tags(sender, instance, **kwargs):
    if not instance.allow_download:
        return
    tags = instance.get_tags()
    tags_all = instance.tagsData.filter(website_id=instance.pk).values_list('tag', flat=True).all()
    tags_to_create = [tag for tag in tags if tag not in tags_all]
    tags_to_delete = [tag for tag in tags_all if tag not in tags]
    instance.tagsData.bulk_create([TagsData(website_id=instance.pk, tag=tag) for tag in tags_to_create])
    instance.tagsData.filter(tag__in=tags_to_delete).delete()
