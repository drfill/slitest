app.factory('STEP1', ['$resource', function ($resource) {
    return $resource(step1_json, null,
        {
            'get': {method: 'GET', isArray: true},
            'update': {method: 'POST', isArray: true}
        });
}])
    .controller('Step1Ctrl', ['$scope', 'STEP1',
        function ($scope, STEP1) {
            $scope.$parent.get_data(STEP1);
        }
    ]);