app.factory('STEP2', ['$resource', function ($resource) {
    return $resource(step2_json, null,
        {
            'get': {method: 'GET', isArray: true}
        });
}])
    .controller('Step2Ctrl', ['$scope', 'STEP2',
        function ($scope, STEP2) {
            $scope.$parent.get_data(STEP2);
        }
    ]);