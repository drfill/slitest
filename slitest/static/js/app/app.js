function angular_url(url) {
    return angular.isDefined(url) ? url.replace(/\/$/g, '/?') : url;
}

var app = angular.module('app', [
    'ngResource'
])
    .controller('AppCtrl', ['$scope', '$resource',
        function ($scope, $resource) {
            $scope.data = [];
            $scope.ws = {};
            $scope.resource = null;

            $scope.get_data = function (resource, callback) {
                $scope.data = resource.get(null, function () {
                    for (var idx in $scope.data) {
                        var d = $scope.data[idx];
                        if (d.hasOwnProperty('fields')) $scope.ws[d.pk] = d.fields.website;
                    }
                });
                $scope.resource = resource;
                if (callback && angular.isFunction(callback)) callback($scope.data);
            };

            $scope.update = function () {
                if (!$scope.resource) return;

                $scope.temp_data = $scope.resource.update(null, function () {
                    for (var idx in $scope.temp_data) {
                        var d = $scope.temp_data[idx];
                        if (d.hasOwnProperty('fields')) {
                            d['fields']['website'] = $scope.ws[d.fields.website];
                            if (!d.fields.data) d['fields']['data'] = 'в процессе...';
                            $scope.temp_data[idx] = d;
                        }
                    }
                    $scope.data = angular.copy($scope.temp_data);
                });
            };
        }])
    .config(['$resourceProvider', function ($resourceProvider) {
        // Don't strip trailing slashes from calculated URLs
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }]);