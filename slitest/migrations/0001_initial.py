# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TagsData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag', models.CharField(max_length=52, verbose_name='Tag', db_index=True)),
                ('data', models.TextField(null=True, verbose_name='Data', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Websites',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('website', models.URLField(unique=True, verbose_name='Website', db_index=True)),
                ('tags', models.CharField(default=b'title', help_text='Divided by semicolon', max_length=52, verbose_name='Tags')),
                ('allow_download', models.BooleanField(default=True, verbose_name='Download?')),
                ('data', models.TextField(verbose_name='Data')),
            ],
            options={
                'verbose_name': 'Website',
                'verbose_name_plural': 'Websites',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='tagsdata',
            name='website',
            field=models.ForeignKey(related_name='tagsData', to='slitest.Websites'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='tagsdata',
            unique_together=set([('website', 'tag')]),
        ),
    ]
