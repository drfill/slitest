from django.contrib.staticfiles.management.commands.runserver import Command as RunserverCommand
from slitest.core.dl import dl


class Command(RunserverCommand):
    def inner_run(self, *args, **kwargs):
        self.stdout.write(''.join(['*' * 15, ' Runnning Async Downloader ', '*' * 15]))
        dl.start_async()
        super(Command, self).inner_run(*args, **kwargs)