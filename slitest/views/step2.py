import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View

from slitest.models.websites import TagsData


class Step2(TemplateView):
    template_name = 'step2.html'


class Step2Json(View):
    http_method_names = ['get']
    data_fields = ['tag', 'data', 'website__website']

    def get(self, request, *args, **kwargs):
        TagsData.update_info()

        return HttpResponse(
            json.dumps(list(TagsData.objects.select_related('websites').values(*self.data_fields).all())),
            content_type='application/json'
        )


step2_url = Step2.as_view()
step2_json = csrf_exempt(Step2Json.as_view())