from django.conf.urls import *


urlpatterns = patterns('slitest.views',
                       url(r'^$', 'index', name='index'),
                       url(r'^step1/$', 'step1.step1_url', name='step1-url'),
                       url(r'^step1/json/$', 'step1.step1_json', name='step1-json'),
                       url(r'^step2/$', 'step2.step2_url', name='step2-url'),
                       url(r'^step2/json/$', 'step2.step2_json', name='step2-json'),
)