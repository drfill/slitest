from django.core import serializers

from django.db.models import Q
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View

from slitest.models.websites import Websites, TagsData


class Step1(TemplateView):
    template_name = 'step1.html'


class Step1Json(View):
    http_method_names = ['get', 'post']
    default_ws = ['ya.ru', 'python.org', 'ru.wikipedia.org']
    default_tag = 'title'

    def get(self, request, *args, **kwargs):
        WS_obj = Websites.objects

        qs, bulk = Q(), []
        for ws in self.default_ws:
            qs |= Q(website__icontains=ws)
            bulk.append(Websites(
                website=ws,
                tags=self.default_tag
            ))
        WS_obj.filter(qs).delete()
        [x.save() for x in bulk]

        data = serializers.serialize(
            'json',
            WS_obj.filter(website__in=self.default_ws).all(),
            fields=('pk', 'website', 'tags')
        )
        return HttpResponse(data, content_type='application/json')

    def post(self, request, *args, **kwargs):
        TagsData.update_info()

        data = serializers.serialize(
            'json',
            TagsData.objects.filter(website__website__in=self.default_ws),
            fields=('website', 'tag', 'data')
        )
        return HttpResponse(data, content_type='application/json')


step1_url = Step1.as_view()
step1_json = csrf_exempt(Step1Json.as_view())